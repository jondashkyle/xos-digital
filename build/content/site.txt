Title: XOS Technology

----

Keywords: Kirby,CMS,file-based

----

Copyright: © 2009-(date: Year) XOS Technology

----

Description: 

----

Footertext: 

(link: /contact#tech-support text: Tech Support) (link: /contact#company text: Company) (link: /contact#contact text: Contact) (link: /contact#tutorials text: Tutorials)

(link: https://twitter.com/xosdigital text: Twitter) (link: https://www.facebook.com/xosdigital text: Facebook) (link: https://www.linkedin.com/company/xos-digital text: Linkedin)

----

Twitter: https://twitter.com/xosdigital

----

Facebook: http://facebook.com/xosdigital

----

Linkedin: https://www.linkedin.com/company/xos-digital