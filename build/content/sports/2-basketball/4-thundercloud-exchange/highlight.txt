Title: ThunderCloud Exchange

----

Text: As coaches are requesting film immediately, the video staff needs faster, more reliable ways to move media without sacrificing ease of use, speed, and content security. XOS ThunderCloud Exchange provides the most efficient means of trading video from anywhere.

----

Blocks: 

- 
  title: Automatic
  snippet: >
    Auto-download content from
    “expected” teams.
- 
  title: Efficient
  snippet: >
    High speed file transfer to and from any
    format.
- 
  title: Universal
  snippet: >
    No hardware to purchase, update, or
    support.
- 
  title: Portable
  snippet: >
    Don’t be restricted to the office.
    Trade video files from anywhere with an
    internet connection.
- 
  title: Proven
  snippet: >
    150+ teams across professional and
    collegiate sports rely on ThunderCloud
    Exchange for fast and reliable video
    exchange.

----

Subtitle: 