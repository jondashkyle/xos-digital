Title: HD Basketball Replay

----

Text: Broadcast quality video, an easy to use single-person interface at an affordable price. No wonder it’s being used at more than 175 facilities across North America alone.

----

Blocks: 

- 
  title: Flexible
  snippet: >
    Review marked plays during or after
    games to ensure accuracy and train
    officials.
- 
  title: Easy to Use
  snippet: Straight-forward, single user interface.
- 
  title: Multi-purpose
  snippet: >
    Live stream, web broadcast, practice
    video or export for post-production
    highlights.
- 
  title: Quality
  snippet: Broadcast quality HD video.

----

Subtitle: 