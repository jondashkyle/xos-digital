Title: Thunder Lacrosse

----

Text: Lacrosse coaches get paid to teach lacrosse – not cut up video. Thunder Lacrosse gives coaches the most robust set of video tools to quickly break down and analyze the exact video they need to see.

----

Blocks: 

- 
  title: Proven
  snippet: >
    Most widely used video editing platform
    at the highest levels of professional
    and amateur sports.
- 
  title: Secure
  snippet: >
    Store your video on-site so your
    coaching staff has immediate access and
    can collaborate – without waiting.
- 
  title: Analyze
  snippet: >
    The most comprehensive analytical and
    reporting tools that allow you to spend
    less time searching and more time
    teaching.
- 
  title: Support
  snippet: >
    World class 24/7/365 support staff with
    experienced industry professionals that
    understand the value of your time.
- 
  title: Integrate
  snippet: >
    Seamless integration with the full suite
    of ThunderCloud solutions to save your
    coaches and players time.