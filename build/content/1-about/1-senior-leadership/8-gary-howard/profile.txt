Title: Gary Howard

----

Position: VP, Scouting Services

----

Text: 

As VP of Scouting Services, Gary Howard is responsible for XOS Digital’s recruiting, scouting, and evaluation solutions for collegiate football programs.

A veteran of the high school football industry, Howard was the Founder and President of NorCal Football Scouting,  a regional content company that provided player data along with high-quality game footage on top student-athletes in California, Arizona, Oregon and Washington.  Since 2001, he has worked with more than 100 college football programs to assist with locating, scouting and evaluating student athletes.

Howard originally entered the digital video and high school content field while working as a football broadcaster for Comcast sports. Prior to that, he spent 15 years as a high school varsity football coach in California.

Howard currently resides in California's North Bay, located in the San Francisco Bay area. He is married to his wife Mimi and they have one son and three daughters.