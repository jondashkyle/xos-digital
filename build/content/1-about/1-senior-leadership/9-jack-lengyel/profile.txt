Title: Jack Lengyel

----

Position: VP, Business Development

----

Text: 

Jack Lengyel has had a long and distinguished career in collegiate athletics. He began his career as the freshman football coach at his alma mater, Akron University, and has been involved with intercollegiate athletics since 1957. Lengyel's diverse career has taken him from the gridiron to the administrative ranks to the private sector.

Lengyel’s greatest task and achievement as a coach occurred between 1971 and 1974, the time he served as head football coach at Marshall University. While coaching at Marshall, he rebuilt the football program and community after a tragic airline crash involving the entire team. In 2006, Warner Brothers released the movie "We Are Marshall" starring Mathew McConaughey in the role of Coach Jack Lengyel.

After three years in the private sector at Gamble Brothers, Lengyel moved into the intercollegiate athletics administrative ranks with roles as Associate Director of Athletics at Louisville and Missouri.  From there, Lengyel has served as the Athletic Director at Fresno State, Missouri, and the United States Naval Academy.  He remained at Navy for 14 years and retired in 2001.  During his tenure at Navy, Lengyel received a Lifetime Service Award from the Annapolis Touchdown Club (2001), was appointed to the U.S. Naval Academy Foundation Board of Trustees (2001) and had the athletic training facility named in his honor.

Following his retirement from Navy, Lengyel has been active as a consultant and serving in leadership positions in various community and professional organizations. He has served as the interim athletics director at Temple University, Eastern Kentucky University and the University of Colorado providing leadership and stability through the administrative transition. Lengyel has used his vast experience to provide strategic operational reviews and analysis for Towson State, Northern Arizona University and Texas State University at San Marcos.