Title: Thomas Lynch

----

Position: The Musser Group

----

Text: 

Adm. Lynch is currently a managing director of The Musser Group, a private equity/venture capital firm in Philadelphia, and a managing director of Jones Lang LaSalle. Prior to that, he was a senior vice president of The Staubach Company since 2001, following six years at Safeguard Scientifics, which included two years as president/COO of CompuCom Systems in Dallas, Texas. While at Safeguard, he was responsible for the operations, mergers and acquisitions, and oversight of other emerging partnership companies.

Adm. Lynch graduated from the U.S. Naval Academy (B.S.) in 1964 and earned an M.S. from George Washington University in 1971. He was recognized as a U.S. Naval Academy Distinguished Graduate in 2010.

----

Subtitle: 

----

Blocks: 

- 
  title: 'Collegiate Career:'
  snippet: >
    Captain of the 1963 U.S. Naval Academy
    football team
- 
  title: 'Service:'
  snippet: Member of the Army-Navy Game Committee
- 
  title: 'Fun Fact:'
  snippet: >
    Played center when Roger Staubach was
    the quarterback at Navy