Title: George Weiss

----

Position: Beechtree Capital

----

Text: 

George M. Weiss, founder and CEO of Beechtree Capital, LLC, is an accomplished attorney, entrepreneur and venture capitalist with more than 40 years of senior management and investing experience with start-up and emerging companies. Weiss has been a company founder, officer and board member as well as a senior advisor to top-level executives in high-growth industries around the world.

His investment and management expertise has contributed to the growth of numerous new or emerging companies in developing industries. Among his many successes have been his contributions to the development and growth of the predecessors of American Tower Corporation and Rogers Cable, Nexsan Technologies, Nexsan Technologies., ArmedZilla, Inc., XOS Digital and numerous other publicly and privately-held companies. Weiss, on behalf of Beechtree Capital, has participated in over 90 venture investments, succeeding in over 85 percent of Beechtree investments where he has served as lead investor.

Weiss currently serves as a founding investor, management advisor and/or board member of seven privately-held Beechtree portfolio companies: Nexsan Technologies, XOS Digital, ArmedZilla, Inc., GoingOn, AlwaysOn, Alternative Alternatives and New University Holdings.

In addition to his accomplished business ventures, Weiss currently serves as vice chairman of the board, committee chairman and senior leader within The National Football Foundation and College Football Hall of Fame, a 60-year old institution promoting athletic and academic excellence. He holds the same titles within the Play It Smart Foundation, a creator and overseer of innovative educational and athletic programs for inner city youth. 

Weiss earned a Masters of Arts degree and a Juris Doctorate degree with honors from New York University and a Bachelor of Arts degree from Bowling Green State University.

----

Subtitle: Tidbits

----

Blocks: 

- 
  title: 'Favorite Teams:'
  snippet: >
    All college football, Dallas Cowboys,
    Denver Broncos, New York Giants, New
    York Knicks, New York Yankees
- 
  title: 'Sports Participation:'
  snippet: Hiking, Tennis
- 
  title: 'Sports Growing Up:'
  snippet: Football, Basketball, Track
- 
  title: 'Fun Fact:'
  snippet: >
    In 2014, George was presented the Gold
    Medal Award by the National Football
    Foundation.