Title: Thunder Hockey

----

Text: 

With 3 games in 4 nights, coaches need video of tonight's game now.  Providing unmatched video editing and analytical tools, XOS Thunder Hockey HD is the premier on-site video coaching solution in the NHL, NCAA, and QMJHL.

(link: /sports/hockey text: More Hockey class: bttn)

----

Blocks: 

- 
  title: Proven
  snippet: >
    Most widely used video editing platform
    in the NHL, NCAA Division 1, and QMJHL
    with more than 100 partners worldwide.
- 
  title: Secure
  snippet: >
    Store your video on-site so your
    coaching staff has immediate access and
    can collaborate – without waiting.
- 
  title: Analyze
  snippet: >
    The most comprehensive analytical and
    reporting tools that allow you to spend
    less time searching and more time
    teaching.
- 
  title: Support
  snippet: >
    World class 24/7/365 support staff with
    experienced industry professionals that
    understand the value of your time.
- 
  title: Integrate
  snippet: >
    Seamless integration with the full suite
    of ThunderCloud solutions to save your
    coaches and players time.

----

Subtitle: 