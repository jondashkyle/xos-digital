Title: Jayse McQuaig Jacksonville State

----

Text: *“The best part about XOS support is these guys understand football.  They understand there is no time off.  It could be 2am but it has to be done.  They get that.”*

----

Source: Jayse McQuaig, Video Director, Jacksonville State Jaguars Football

----

Sport: Football