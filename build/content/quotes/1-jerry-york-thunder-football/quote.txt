Title: Jerry York

----

Text: *"Thunder Hockey and the quality of its high-definition video has provided Boston College hockey with a great competitive advantage for breakdown and analysis."*

----

Source: Jerry York, Head Coach, Boston College Eagles Hockey

----

Sport: Hockey