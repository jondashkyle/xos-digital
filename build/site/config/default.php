<?

/**
 * Checks if the page has a mp4, webm, and gif
 *
 * @return boolean
 */
function hasAllMedia($pageObject){

  $has = array(
    'mp4'  => null,
    'webm' => null,
    'gif'  => null
  );

  foreach($pageObject->files() as $file) {
    $has[$file->extension()] = true;
  }

  return isAllTrue($has);

};