<?php if(!defined('KIRBY')) exit ?>

username: xos
email: xos@jon-kyle.com
password: >
  $2a$10$4GFl6IVWQfugU1sE399O2eo3TmAY7ealxGPzWPI7waiRUY1IVTnHy
language: en
role: admin
token: 3c915388d48dd819d06d811ec4a15e4ef940912c
history:
  - contact
  - null
  - content-licensing/home-block
  - about/strategic-partner-network
  - >
    about/strategic-partner-network/why-partner-with-xos
