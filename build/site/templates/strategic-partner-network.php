<?php snippet('header') ?>

<div class="row background-black type-white margin-nav-top">
  <div class="col-xs-12 pad-xs-big pad-bottom-small type-center">
    <h2 class="type-light"><?= $page->title() ?></h2>
  </div>

  <div id="about" class="col-xs-12 col-md-10 col-md-offset-1 pad-xs-small pad-md-big type-center">
    <div class="type-center type-normalish"><?= $page->text()->kirbytext() ?></div>
  </div>

  <div id="about" class="col-xs-12 col-md-10 col-md-offset-1 pad-xs-small pad-md-big type-center">
    <h2>About</h2>
    <div class="type-center type-normalish"><?= $page->about()->kirbytext() ?></div>
  </div>
</div>

<div
  class="row list-padded"
  data-desc="Products and Services"
>
  <div class="col-xs-12">
    <?
      foreach ($page->children()->visible() as $p) {
        snippet('highlight-fit', array(
          'p' => $p
        ));
      }
    ?>
  </div>
</div>

<div class="row background-black type-white">
  <div id="about" class="col-xs-12 col-md-10 col-md-offset-1 pad-xs-small pad-md-big type-center">
    <h2>Contact</h2>
    <div class="type-center type-normalish"><?= $page->contact()->kirbytext() ?></div>
  </div>
</div>

<?php snippet('footer') ?>
