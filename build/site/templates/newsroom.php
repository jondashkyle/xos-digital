<?php snippet('header') ?>

<div class="row background-black type-white margin-nav-top">
  <div class="col-xs-12 pad-xs-big pad-bottom-small type-center">
    <h2 class="type-light"><?= $page->title() ?></h2>
  </div>
  <div class="col-xs-12 col-md-10 col-md-offset-1 pad-xs-small pad-md-big">
    <div class="type-center type-normalish"><?= $page->text() ?></div>
  </div>
</div>


<div id="in-the-company" class="row background-black type-white">
  <div class="col-xs-12 pad-xs-big pad-bottom-small type-center">
    <h2>In the Company</h2>
  </div>
  <div
    class="col-xs-12 col-lg-10 col-lg-offset-1 pad-xs-small pad-lg-big pad-bottom-reset"
    data-paginate-list
  >
    <? foreach ($page->find('in-the-company')->children()->visible() as $block): ?>
    <div class="row margin-bottom-small" data-paginate-list-item>
      <div class="col-xs-2 col-lg-4 type-caps type-condensed type-normalish">
        <?= $block->date('M Y') ?>
      </div>
      <div class="col-xs-10 col-lg-8">
        <p>
        <? if ($block->hasFiles()): ?>
          <a href="<?= $block->files()->first()->url() ?>" target="_blank">
        <? endif; ?>
          <?= $block->text()->html() ?>
        <? if ($block->hasFiles()): ?>
          </a>
        <? endif; ?>
        </p>
      </div>
    </div>
    <? endforeach; ?>
    <div class="row">
      <div class="col-xs-12 type-center pad-xs-big">
        <div>
          <div class="bttn" data-paginate-list-more="6">Load more</div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="in-the-news" class="row background-black type-white">
  <div class="col-xs-12 pad-xs-big pad-bottom-small type-center">
    <h2>In the News</h2>
  </div>
  <div
    class="col-xs-12 col-lg-10 col-lg-offset-1 pad-xs-small pad-lg-big pad-bottom-reset"
    data-paginate-list
  >
    <? foreach ($page->find('in-the-news')->children()->visible() as $block): ?>
    <div class="row margin-bottom-small" data-paginate-list-item>
      <div class="col-xs-2 col-lg-4 type-caps type-condensed type-normalish">
        <?= $block->date('M Y') ?>
      </div>
      <div class="col-xs-10 col-lg-8">
        <?= $block->text()->kirbytext() ?>
      </div>
    </div>
    <? endforeach; ?>
    <div class="row">
      <div class="col-xs-12 type-center pad-xs-big">
        <div>
          <div class="bttn" data-paginate-list-more="6">Load more</div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="media-library" class="row center-xs">
  <div class="col-xs-12 pad-xs-big type-center">
    <h2>Media Library</h2>
  </div>
</div>

<div class="row center-xs list-padded container-pad-small" data-paginate-list>
  <? foreach ($page->find('media-library')->images() as $p): ?>
  <? if ($p->video() != ''): ?>
  <div class="col-xs-6 col-md-4 pad-xs-small" data-paginate-list-item>
    <div class="container-video" data-video-inline>
      <?= vimeo($p->video()) ?>
      <img src="<?= thumb($p, array('height' => 800, 'width' => 800), false) ?>" alt="">
    </div>
  </div>
  <? endif; ?>
  <? endforeach; ?>
  <div class="row">
    <div class="col-xs-12 type-center pad-xs-big type-black">
      <div>
        <div class="bttn" data-paginate-list-more="6">Load more</div>
      </div>
    </div>
  </div>
</div>

<div class="row background-gray">
  <div class="col-xs-12 pad-xs-big pad-bottom-reset type-center type-white">
    <h4>More About Us</h4>
  </div>
  <div class="col-sm-12 col-sm-offset-0 col-lg-10 col-lg-offset-1 pad-xs-big">
    <div class="row">
      <div class="col-xs-12 col-sm-6 pad-xs-medium">
        <a href="<?= page('about')->url() ?>" class="bttn-big"><?= page('about')->titlealt() ?></a>
      </div>
      <div class="col-xs-12 col-sm-6 pad-xs-medium">
        <a href="<?= page('about')->find('partners')->url() ?>" class="bttn-big">Partners</a>
      </div>
    </div>
  </div>
</div>

<?php snippet('footer') ?>
