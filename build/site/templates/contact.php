<?php snippet('header') ?>

<div id="general" class="row background-black type-white margin-nav-top">
  <div id="tech-support" class="col-xs-12 pad-xs-big pad-bottom-small type-center">
    <h2 class="type-light"><?= $page->helptitle()->html() ?></h2>
  </div>

  <div id="about" class="col-xs-12 col-md-10 col-md-offset-1 pad-xs-small pad-md-big">
    <div class="type-center type-normalish"><?= $page->helptext()->kirbytext() ?></div>
  </div>

  <div class="col-xs-12 type-center">
    <div
      class="row pad-bottom-big center-xs type-white container-pad-small stack-2"
      style="position: relative;"
    >
      <div class="pad-xs-big type-center col-xs-12">
        <h4 class="type-grey"><?= $page->helpsubtitle() != '' ? $page->helpsubtitle() : 'Or Reach Us Here' ?></h4>
      </div>
      <? foreach ($page->helpoptions()->yaml() as $block): ?>
      <div class="col-xs-12 col-sm-6 col-lg-4 pad-xs-medium">
        <h3><?= $block['title'] ?></h3>
        <?= kirbytext($block['snippet']) ?>
      </div>
      <? endforeach; ?>
    </div>
  </div>

  <div id="company" class="col-xs-12 pad-xs-big pad-bottom-small type-center">
    <h2 class="type-light"><?= $page->jointitle()->html() ?></h2>
  </div>

  <div class="col-xs-12 col-md-10 col-md-offset-1 pad-xs-small pad-md-big">
    <div class="type-center type-normalish"><?= $page->jointext()->kirbytext() ?></div>
  </div>

  <div id="contact" class="col-xs-12 pad-xs-big pad-bottom-small type-center">
    <h2 class="type-light"><?= $page->contacttitle()->html() ?></h2>
  </div>

  <div class="col-xs-12 col-md-10 col-md-offset-1 pad-xs-small pad-md-big">
    <div class="type-center type-normalish"><?= $page->contacttext()->kirbytext() ?></div>
  </div>

  <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 pad-xs-medium pad-bottom-big">
    <div class="row form">
      <div class="col-xs-6 pad-xs-small">
        <input type="text" placeholder="Name">
      </div>
      <div class="col-xs-6 pad-xs-small">
        <input type="text" placeholder="Company">
      </div>
      <div class="col-xs-6 pad-xs-small">
        <input type="text" placeholder="Email">
      </div>
      <div class="col-xs-6 pad-xs-small">
        <input type="text" placeholder="Phone Number">
      </div>

      <div class="col-xs-12 pad-xs-small" style="position: relative">
        <div class="contact-arrow"></div>
        <select>
          <option value="" disabled selected>Category</option>
          <option value="General">General</option>
          <option value="Game Prep Sales">Game Prep Sales</option>
          <option value="Content Licensing Sales">Content Licensing Sales</option>
          <option value="Careers">Careers</option>
          <option value="Other">Other</option>
        </select>
      </div>

      <div class="col-xs-12 pad-xs-small">
        <textarea name="" id="" rows="10" placeholder="Comments"></textarea>
      </div>

      <div class="col-xs-12 pad-xs-medium">
        <div class="type-center"><button class="bttn">Submit</button></div>
      </div>
    </div>
  </div>

  <div class="col-xs-12 type-center">
    <div
      class="row pad-bottom-big center-xs type-white container-pad-small stack-2"
      style="position: relative;"
    >
      <div class="pad-xs-big type-center col-xs-12">
        <h4 class="type-grey"><?= $page->optionstitle() != '' ? $page->optionstitle() : 'Or Reach Us Here' ?></h4>
      </div>
      <? foreach ($page->contactoptions()->yaml() as $block): ?>
      <div class="col-xs-12 col-sm-6 col-lg-4 pad-xs-medium">
        <h3><?= $block['title'] ?></h3>
        <?= kirbytext($block['snippet']) ?>
      </div>
      <? endforeach; ?>
      <div class="col-xs-12 col-sm-6 col-lg-4 pad-xs-medium">
        <h3>Social media</h3><br/>
        <ul class="pad-xs-medium pad-top-reset list-inline type-caps type-condensed type-normal-medium type-center">
          <li><a href="<?= $site->facebook() ?>" class="social facebook" target="_blank">Facebook</a></li>
          <li><a href="<?= $site->twitter() ?>" class="social twitter" target="_blank">Twitter</a></li>
          <li><a href="<?= $site->linkedin() ?>" class="social linkedin" target="_blank">Linkedin</a></li>
        </ul>
      </div>
    </div>
  </div>

  <div id="tutorials" class="col-xs-12 pad-xs-big pad-bottom-small type-center">
    <h2 class="type-light"><?= $page->tutorialstitle()->html() ?></h2>
  </div>

  <div class="col-xs-12 col-md-10 col-md-offset-1 pad-xs-small pad-md-big">
    <div class="type-center type-normalish"><?= $page->tutorialstext()->kirbytext() ?></div>
  </div>

  <div class="col-xs-12 pad-bottom-big">
    <div></div>
  </div>

</div>

<?php snippet('footer') ?>
