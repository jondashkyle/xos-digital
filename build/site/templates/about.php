<?php snippet('header') ?>

<div id="who-we-are" class="row background-black type-white margin-nav-top">
  <div class="col-xs-12 pad-xs-big pad-bottom-small type-center">
    <h2 class="type-light"><?= $page->titlealt() ?></h2>
  </div>
  <div id="about" class="col-xs-12 col-md-10 col-md-offset-1 pad-xs-small pad-md-big">
    <div class="type-center type-normalish"><?= $page->text() ?></div>
  </div>
</div>

<div id="history" class="row background-black type-white">
  <div class="col-xs-12 pad-xs-big pad-bottom-small type-center">
    <h2>History</h2>
  </div>
  <div
    class="col-xs-12 col-lg-10 col-lg-offset-1 pad-xs-small pad-lg-big pad-bottom-reset"
    data-paginate-list
  >
    <? foreach ($page->history()->yaml() as $block): ?>
    <div class="row margin-bottom-small" data-paginate-list-item>
      <div class="col-xs-2 col-lg-4 type-caps type-condensed type-normalish">
        <?= date('M Y', strtotime($block['date'])) ?>
      </div>
      <div class="col-xs-10 col-lg-8">
        <?= kirbytext($block['text']) ?>
      </div>
    </div>
    <? endforeach; ?>
    <div class="row">
      <div class="col-xs-12 type-center pad-xs-big">
        <div>
          <div class="bttn" data-paginate-list-more="6">Load more</div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="<?= $page->find('senior-leadership')->uid() ?>">
  <div class="row center-xs">
    <div class="col-xs-12 pad-xs-big type-center">
      <h2>Senior Leadership</h2>
    </div>
  </div>

  <div class="row center-xs list-padded container-pad-small">
    <? foreach ($page->find('senior-leadership')->children()->visible() as $p): ?>
    <div class="col-xs-4 col-md-3 pad-xs-small img-desaturated-hover">
      <div>
        <div
          class="img-square type-bold"
          <? if ($p->images()->first()) echo 'style="background-image: url(' . thumb($p->images()->first(), array('height' => 800, 'width' => 800), false) . ')"' ?>
        >
          <a
            href="<?= $p->url() ?>"
            class="type-white type-bold block-overlay block-overlay-bottom stack-5"
          >
            <div class="title-bg"><?= $p->title() ?></div>
          </a>
        </div>
      </div>
    </div>
    <? endforeach; ?>
  </div>
</div>

<div id="<?= $page->find('board-of-directors')->uid() ?>">
  <div class="row center-xs">
    <div class="col-xs-12 pad-xs-big type-center">
      <h2>Board of Directors</h2>
    </div>
  </div>

  <div class="row list-padded container-pad-small center-xs">
    <? foreach ($page->find('board-of-directors')->children()->visible() as $p): ?>
    <div class="col-xs-4 col-md-3 pad-xs-small img-desaturated-hover">
      <div>
        <div
          class="img-square type-bold"
        >
          <a
            href="<?= $p->url() ?>"
            class="type-white block-overlay pad-xs-medium stack-5"
          >
            <div>
              <span class="type-bold"><?= $p->title() ?></span><br/>
              <?= $p->position() ?>
            </div>
          </a>
        </div>
      </div>
    </div>
    <? endforeach; ?>
  </div>
</div>

<div id="team-xos-highlights">
  <?
    snippet('quotes', array(
      'p' => page('about')->find('team-xos-highlights'),
      'size' => 'type-normal'
    ))
  ?>
</div>

<div class="row background-gray">
  <div class="col-xs-12 pad-xs-big pad-bottom-reset type-center type-white">
    <h4>More About Us</h4>
  </div>
  <div class="col-sm-12 col-sm-offset-0 col-lg-10 col-lg-offset-1 pad-xs-big">
    <div class="row">
      <div class="col-xs-12 col-sm-6 pad-xs-medium">
        <a href="<?= page('newsroom')->url() ?>" class="bttn-big">News Room</a>
      </div>
      <div class="col-xs-12 col-sm-6 pad-xs-medium">
        <a href="<?= page('about')->find('partners')->url() ?>" class="bttn-big">Partners</a>
      </div>
    </div>
  </div>
</div>

<?php snippet('footer') ?>
