<?php snippet('header') ?>

<?
  $header = $page->images()->find('header.jpg');
  if (! $header) {
    $header = $page->images()->find('thumbnail.jpg');
  }
  if (! $header) {
    $header = $page->images()->first();
  }
?>

<div class="row">
  <div
    class="block-cover"
    <? if ($header): ?>
    style="background-image: url(<?= thumb($header, array('height' => 1600, 'width' => 1600), false) ?>)"
    <? endif; ?>
  >
    <? if ($page->images()->find('icon.svg')): ?>
    <div class="block-overlay pad-xs-medium stack-3">
      <img src="<?= $page->images()->find('icon.svg')->url() ?>" class="img-cover " data-inject>
    </div>
    <? endif; ?>
    <div class="pad-xs-medium block-overlay block-overlay-bottom type-white stack-2">
      <div>
        <h2><?= $page->title() ?></h2>
      </div>
    </div>
    <? if ($page->video() != ''): ?>
      <div
        class="header-video"
        data-video="<?= $page->video() ?>"
      >
        <div data-video-container></div>
      </div>
    <?php endif; ?>
    <div class="gradient-bottom-black"></div>
  </div>
</div>

<div class="row type-center background-black type-white">
  <div class="col-xs-12 col-md-10 col-xs-offset-0 col-md-offset-1 pad-xs-medium pad-bottom-big type-normalish">
    <?= $page->text()->kirbytext() ?>
  </div>
</div>

<? if ($page->find('partners')->hasImages()): ?>
<div class="row list-padded container-pad-small">
  <div class="pad-xs-medium type-center col-xs-12">
    <h4 class="type-grey"><?= $page->title() ?> Partners</h4>
  </div>

<div class="col-xs-12">
  <div class="pad-xs-medium pad-reset-top type-black type-center slideshow">
    <div class="slick-gradient-white"></div>
      <div data-slideshows-partners>
        <? foreach($page->find('partners')->images()->shuffle() as $img): ?>
        <div class="pad-xs-small">
          <div
            class="img-thumb"
            style="background-image: url(<?= $img->url() ?>)"
          ></div>
        </div>
        <? endforeach; ?>
      </div>
    </div>
    <div class="pad-xs-medium pad-xs-top-none type-black type-center">
      <a href="<?= page('about')->find('partners')->url() ?>" class="bttn">View All</a>
    </div>
  </div>
</div>
<?php endif; ?>

<div
  class="row list-padded container-pad-small"
  data-desc="Products and Services"
>
  <div class="pad-xs-small col-xs-12">
    <?
      foreach ($page->children()->visible() as $p) {
        snippet('highlight-fit', array(
          'p' => $p
        ));
      }
    ?>
  </div>
</div>

<?
  snippet('quotes', array(
    'tag' => $page->title()
  ))
?>

<?php snippet('footer') ?>
