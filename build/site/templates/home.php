<?php snippet('header') ?>

<div
  class="row"
  data-desc="Homepage header"
>
  <div
    class="block-cover"
    style="background-image: url(<?= thumb($page->images()->find('header.jpg'), array('height' => 1600, 'width' => 1600), false) ?>)"
  >
    <div class="logo-home stack-6">
      <img src="<?= url('/assets/images/xos-big.svg') ?>" alt="">
    </div>

    <? if ($page->video() != ''): ?>
      <div
        class="header-video"
        data-video="<?= $page->video() ?>"
      >
        <div data-video-container></div>
      </div>
    <?php endif; ?>
  </div>
  <?
    snippet('block-cover', array(
      'p' => page('about')->find('home-block')
    ))
  ?>
</div>

<div class="row">
  <div class="col-xs-12">
    <div>
      <div class="col-xs-12 type-center pad-xs-medium pad-bottom-medium">
        <h4 class="type-grey">Team XOS</h4>
      </div>
      <div class="pad-xs-medium pad-reset-top type-black type-center slideshow">
        <div class="slick-gradient-white"></div>
        <div data-slideshows-partners>
          <? foreach(page('sports')->children()->visible()->shuffle() as $p): ?>
            <? foreach($p->find('partners')->images()->shuffle()->limit(10) as $img): ?>
            <div class="pad-xs-small">
              <div
                class="img-thumb"
                style="background-image: url(<?= $img->url() ?>)"
              ></div>
            </div>
            <? endforeach; ?>
          <? endforeach; ?>
        </div>
      </div>
      <div class="pad-xs-medium pad-xs-top-none type-black type-center">
        <a href="<?= page('about')->find('partners')->url() ?>" class="bttn">View All</a>
      </div>
    </div>
  </div>
</div>

<div
  class="row list-padded container-pad-small background-black"
  data-desc="Products and Services"
>
  <div class="pad-xs-small col-xs-12">
    <div class="pad-xs-big type-center">
      <h4 class="type-grey">Products and Services</h4>
    </div>
  </div>
  <? foreach ($pages->find('game-prep', 'content-licensing', 'scouting-recruiting') as $p): ?>
    <? if ($p->find('home-block')): ?>
    <div class="pad-xs-small col-xs-12">
      <?
        snippet('block-cover', array(
          'p' => $p->find('home-block')
        ))
      ?>
    </div>
    <? endif; ?>
  <? endforeach; ?>

</div>

<div class="row list-padded container-pad-small background-gray-light">
  <div class="pad-xs-small col-xs-12">
    <div class="pad-xs-big type-center">
      <h4 class="type-grey">Sports</h4>
    </div>
  </div>
  <div class="col-xs-12 background-black">
    <div class="row center-xs">
      <? foreach (page('sports')->children()->visible()->not('baseball') as $p): ?>
      <div class="col-xs-6 col-md-4 pad-xs-small">
        <div>
          <div
            class="img-sport block-dim type-bold"
            <? if ($p->images()->find('thumbnail.jpg')) echo 'style="background-image: url(' . thumb($p->images()->find('thumbnail.jpg'), array('height' => 800, 'width' => 800), false) . ')"' ?>
          >
            <? if ($p->images()->find('icon.svg')): ?>
              <div class="block-overlay block-overlay-topish pad-xs-medium stack-3">
                <img src="<?= $p->images()->find('icon.svg')->url() ?>" data-inject>
              </div>
            <? endif; ?>
            <a
              href="<?= $p->url() ?>"
              class="type-white type-bold block-overlay block-overlay-bottom pad-xs-medium stack-5"
            >
              <div><?= $p->title() ?></div>
            </a>
          </div>
        </div>
      </div>
      <? endforeach; ?>
    </div>
  </div>

</div>

<div class="row list-padded container-pad-small">
  <div class="pad-xs-small col-xs-12">
    <div class="pad-xs-big type-center">
      <h4 class="type-grey">Partners</h4>
    </div>
  </div>

  <div class="pad-xs-small col-xs-12">
  <?
    snippet('block-cover', array(
      'p' => page('about')->find('partners')->find('home-block')
    ))
  ?>
  </div>
</div>

<? snippet('quotes') ?>

<?php snippet('footer') ?>
