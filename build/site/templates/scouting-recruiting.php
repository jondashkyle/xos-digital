<?php snippet('header') ?>

<div class="row background-black type-white margin-nav-top">
  <div class="col-xs-12 pad-xs-big pad-bottom-small type-center">
    <h2 class="type-light"><?= $page->title() ?></h2>
  </div>
  <div id="about" class="col-xs-12 col-md-10 col-md-offset-1 pad-xs-small pad-md-big">
    <div class="type-center type-normalish"><?= $page->text() ?></div>
  </div>
</div>

<div
  class="row list-padded container-pad-small"
  data-desc="Products and Services"
>
  <div class="pad-xs-small col-xs-12">
    <?
      foreach ($page->children()->visible() as $p) {
        snippet('highlight-fit', array(
          'p' => $p
        ));
      }
    ?>
  </div>
</div>

<div class="row background-gray">
  <div class="col-xs-12 pad-xs-big pad-bottom-reset type-center type-white">
    <h4>More Products and Services</h4>
  </div>
  <div class="col-sm-12 col-sm-offset-0 col-lg-10 col-lg-offset-1 pad-xs-big">
    <div class="row">
      <div class="col-xs-12 col-sm-6 pad-xs-medium">
        <a href="<?= page('game-prep')->url() ?>" class="bttn-big"><?= page('game-prep')->title() ?></a>
      </div>
      <div class="col-xs-12 col-sm-6 pad-xs-medium">
        <a href="<?= page('content-licensing')->url() ?>" class="bttn-big"><?= page('content-licensing')->title() ?></a>
      </div>
    </div>
  </div>
</div>

<?php snippet('footer') ?>
