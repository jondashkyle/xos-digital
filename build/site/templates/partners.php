<?php snippet('header') ?>
<?
  $partners = new Pages();
  $images = array();

  foreach ($page->images() as $img) {
    $images[] = $img;
  }

  foreach (page('sports')->children()->visible() as $sport) {
    foreach ($sport->find('partners')->images() as $img) {
      $images[] = $img;
    }
  }
?>

<div class="row background-black type-white margin-nav-top">
  <div class="col-xs-12 pad-xs-big pad-bottom-small type-center">
    <h2 class="type-light"><?= $page->title() ?></h2>
  </div>
  <div id="about" class="col-xs-12 col-md-10 col-md-offset-1 pad-xs-small pad-md-big">
    <div class="type-center type-normalish"><?= $page->text() ?></div>
  </div>
</div>

<div class="row center-xs list-padded container-pad-small pad-xs-big">
  <? foreach ($images as $img): ?>
    <div class="col-xs-2 col-md-1 pad-xs-small">
      <div>
        <div 
          class="img-thumb"
          <?= 'style="background-image: url(' . thumb($img, array('height' => 200, 'width' => 200), false) . ')"' ?>
        ></div>
      </div>
    </div>
  <? endforeach; ?>
</div>

<? 
  snippet('quotes') 
?>

<div class="row background-gray">
  <div class="col-xs-12 pad-xs-big pad-bottom-reset type-center type-white">
    <h4>More About Us</h4>
  </div>
  <div class="col-sm-12 col-sm-offset-0 col-lg-10 col-lg-offset-1 pad-xs-big">
    <div class="row">
      <div class="col-xs-12 col-sm-6 pad-xs-medium">
        <a href="<?= page('about')->url() ?>" class="bttn-big"><?=  page('about')->titlealt() ?></a>
      </div>
      <div class="col-xs-12 col-sm-6 pad-xs-medium">
        <a href="<?= page('newsroom')->url() ?>" class="bttn-big"><?= page('newsroom')->title() ?></a>
      </div>
    </div>
  </div>
</div>

<?php snippet('footer') ?>