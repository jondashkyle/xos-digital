<?php snippet('header') ?>

<div id="<?= $page->uid() ?>" class="background-black type-white margin-bottom-small margin-nav-top">
  <div class="background-black">
      <div class="row">
        <div class="col-xs-12 type-center pad-xs-medium" style="padding-top: 6rem;">
          <h2 class="type-medium"><?= $page->title() ?></h2>
        </div>
      </div>
      <div class="row">
        <? if ($page->hasImages()): ?>
        <div class="col-xs-3 col-xs-offset-1 pad-xs-medium">
          <div 
            class="img-square background-dark-gray"
            <? if ($page->hasImages()) echo 'style="background-image: url(' . thumb($page->images()->first(), array('height' => 800, 'width' => 800), false) . ')"' ?>
          >
          </div>
        </div>

        <div class="col-xs-7 pad-xs-medium type-normal">
          <?= $page->text()->kirbytext() ?>
        </div>
        <? else: ?>
        <div class="col-xs-3 col-xs-offset-1 pad-xs-medium type-normal">
          <? if ($page->blocks()->yaml()): ?>
            <? foreach ($page->blocks()->yaml() as $block): ?>
            <div style="margin-bottom: 1em;">
              <?= $block['title'] ?>
              <em><?= $block['snippet'] ?></em>
            </div>
            <? endforeach; ?>
          <? endif; ?>
        </div>

        <div class="col-xs-7 pad-xs-medium type-normal">
          <?= $page->text()->kirbytext() ?>
        </div>
        <? endif; ?>
      </div>
    </div>

    <div style="height: 8vh">&nbsp;</div>
    <div class="row">
      <div class="col-xs-12">
        <div class="type-center">
          <a href="<?= page('about')->url() ?>#senior-leadership" class="bttn">View All Leadership</a>
        </div>
      </div>
    </div>
    <div style="height: 10vh">&nbsp;</div>
  </div>
</div>

<?php snippet('footer') ?>