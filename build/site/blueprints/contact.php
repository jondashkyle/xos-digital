<?php if(!defined('KIRBY')) exit ?>

title: Contact
pages: false
files: false
fields:
  title:
    label: Title
    type:  text
  contacttitle:
    label: Contact Title
    type: text
  contacttext:
    label: Contact Text
    type:  textarea
  optionstitle:
    label: Options title
    type:  text
  contactoptions:
    label: Options
    type: structure
    entry: >
      <h2>{{title}}</h2><br/>
      {{snippet}}
    fields:
      title:
        label: Title
        type: text
      snippet:
        label: Snippet
        type: textarea
  helptitle:
    label: Help title
    type:  text
    width: 1/2
  helpsubtitle:
    label: Help sub-title
    type:  text
    width: 1/2
  helptext:
    label: Help Text
    type:  textarea
  helpoptions:
    label: Help Options
    type: structure
    entry: >
      <h2>{{title}}</h2><br/>
      {{snippet}}
    fields:
      title:
        label: Title
        type: text
      snippet:
        label: Snippet
        type: textarea
  jointitle:
    label: Join title
    type:  text
  jointext:
    label: Join Text
    type:  textarea
  tutorialstitle:
    label: Tutorials Title
    type: text
  tutorialstext:
    label: Tutorials Text
    type:  textarea
