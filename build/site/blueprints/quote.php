<?php if(!defined('KIRBY')) exit ?>

title: Quote
pages: false
files: true
fields:
  title:
    label: Title
    type: text
    width: 1/2
  sport:
    label: Sport
    type: text
    width: 1/2
  text:
    label: Quote
    type: textarea
  source:
    label: Source
    type: text