<?php if(!defined('KIRBY')) exit ?>

title: Profiles
pages:
  template:
    - profile
files: true
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea