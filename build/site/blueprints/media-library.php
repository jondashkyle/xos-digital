<?php if(!defined('KIRBY')) exit ?>

title: Media Library
pages: false
files:
  fields:
    video:
      label: Video
      type: text
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea