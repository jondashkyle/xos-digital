<?php if(!defined('KIRBY')) exit ?>

title: Update
pages: false
files: true
fields:
  title:
    label: Title
    type: text
    width: 1/2
  date:
    label: Date
    type: date
    format: MM/YYYY
    width: 1/2
  text:
    label: Text
    type: textarea