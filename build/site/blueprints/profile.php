<?php if(!defined('KIRBY')) exit ?>

title: Profile
pages: false
files: true
fields:
  title:
    label: Title
    type:  text
    width: 1/2
  position:
    label: Position
    type:  text
    width: 1/2
  text:
    label: Text
    type:  textarea
  subtitle:
    label: Subtitle
    type: text
  blocks:
    label: Blocks
    type: structure
    entry: >
      <h2>{{title}}</h2><br/>
      {{snippet}}
    fields:
      title:
        label: Title
        type: text
      snippet:
        label: Snippet
        type: textarea