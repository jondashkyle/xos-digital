<?php if(!defined('KIRBY')) exit ?>

title: About
pages: true
  template:
    - page
files: false
fields:
  title:
    label: Title
    type:  text
    width: 1/2
  titlealt:
    label: Alternate title
    type:  text
    width: 1/2
  text:
    label: Text
    type:  textarea
  history:
    label: History
    type: structure
    entry: >
      <h2>{{date}}</h2><br/>
      {{text}}
    fields:
      date:
        label: Date
        type: date
        format: MM/YYYY
      text:
        label: Text
        type: textarea