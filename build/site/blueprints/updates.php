<?php if(!defined('KIRBY')) exit ?>

title: Updates
pages:
  template:
    - update
files: true
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea