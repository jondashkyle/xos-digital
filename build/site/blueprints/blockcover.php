<?php if(!defined('KIRBY')) exit ?>

title: Page
pages: true
  template:
    - false
files: true
fields:
  title:
    label: Title
    type:  text
  video:
    label: Video URL
    type: text
    placeholder: Copy and paste the full Vimeo URL here
  text:
    label: Text
    type:  textarea