<?php if(!defined('KIRBY')) exit ?>

title: Site
pages: default
fields:
  title:
    label: Title
    type:  text
  description:
    label: Description
    type:  textarea
  keywords:
    label: Keywords
    type:  tags
  copyright:
    label: Copyright
    type:  textarea
  footertext:
    label: Footer Text
    type: textarea
  twitter:
    label: Twitter
    type:  text
  facebook:
    label: Facebook
    type:  text
  linkedin:
    label: Linkedin
    type:  text