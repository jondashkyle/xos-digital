<?php if(!defined('KIRBY')) exit ?>

title: Quotes
pages: 
  template:
    - quote
files: false
fields:
  title:
    label: Title
    type:  text
  info:
    type: info
    text: >
      You can add quotes by creating pages to the left. These can contain images and any other content usable within Highlights blocks.