<?php if(!defined('KIRBY')) exit ?>

title: Highlight
pages: false
files: true
fields:
  title:
    label: Title
    type: text
    width: 1/2
  subtitle:
    label: Subtitle
    type: text
    width: 1/2
  text:
    label: Description
    type:  textarea
  blocks:
    label: Blocks
    type: structure
    entry: >
      <h2>{{title}}</h2><br/>
      {{snippet}}
    fields:
      title:
        label: Title
        type: text
      snippet:
        label: Snippet
        type: textarea