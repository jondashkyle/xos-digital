<?php if(!defined('KIRBY')) exit ?>

title: Scouting and Recruiting
pages: 
  template:
    - highlight
files: true
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea