<?php if(!defined('KIRBY')) exit ?>

title: Page
pages: true
  template:
    - highlight
files: true
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea
  about:
    label: About
    type: textarea
  contact:
    label: Contact
    type: textarea