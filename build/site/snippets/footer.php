  <footer>
    <div class="row">
      <div class="site-footer pad-xs-big type-center col-xs-12">
        <div class="type-grey">
          <h2>Get in Touch</h2>
          <ul class="pad-xs-medium list-inline type-caps type-condensed type-normal-medium type-center">
            <li><a href="<?= page('contact')->url() ?>#tech-support">Tech Support</a></li>
            <li><a href="<?= page('contact')->url() ?>#company">Company</a></li>
            <li><a href="<?= page('contact')->url() ?>#contact">Contact</a></li>
            <li><a href="<?= page('contact')->url() ?>#tutorials">Tutorials</a></li>
          </ul>
          <ul class="pad-xs-medium pad-top-reset list-inline type-caps type-condensed type-normal-medium type-center">
            <li><a href="<?= $site->facebook() ?>" class="social facebook" target="_blank">Facebook</a></li>
            <li><a href="<?= $site->twitter() ?>" class="social twitter" target="_blank">Twitter</a></li>
            <li><a href="<?= $site->linkedin() ?>" class="social linkedin" target="_blank">Linkedin</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <script src="//f.vimeocdn.com/js/froogaloop2.min.js"></script>
  <?php echo js('bundles/bundle.js') ?>
</body>
</html>
