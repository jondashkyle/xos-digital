<?
  $background = $p && $p->hasImages() && $p->images()->find('background.jpg');
?>

<div id="<?= $p->uid() ?>" class="margin-bottom-small">
  <div class="background-black gradient-container">
    <div
      class="type-white <? if ($background) echo 'block-dim' ?>"
      <? if ($background) echo 'style="background-image: url(' . thumb($p->images()->find('background.jpg'), array('height' => 1600, 'width' => 1600), false) . ')"' ?>
    >
      <div class="stack-4 type-center stack-position pad-xs-big">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
          <h2><?= $p->title() ?></h2>
          <div class="type-normalish">
            <?= $p->text()->kirbytext() ?>
          </div>
        </div>
      </div>

      <div class="gradient-bottom-black"></div>
    </div>

    <div
      class="row pad-bottom-big center-xs type-white container-pad-small stack-2"
      style="position: relative;"
    >
      <? if ($p->subtitle() != ''): ?>
      <div class="pad-xs-big type-center col-xs-12">
        <h4 class="type-grey"><?= $p->subtitle() ?></h4>
      </div>
      <? endif; ?>
      <? foreach ($p->blocks()->yaml() as $block): ?>
      <div class="col-xs-12 col-sm-6 col-lg-4 pad-xs-medium">
        <h3><?= $block['title'] ?></h3>
        <?= kirbyText($block['snippet']) ?>
      </div>
      <? endforeach; ?>
    </div>
  </div>
</div>
