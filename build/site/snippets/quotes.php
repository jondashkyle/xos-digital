<?
  $parent = (isset($p) ? $p : page('quotes'));
  $children = (isset($tag) ? $parent->children()->visible()->filterBy('sport', $tag, ',') : $parent->children()->visible());

  if ($children->count() == 0) {
      $children = $parent->children()->visible();
  }
?>

<? if ($children): ?>
<div class="row background-black type-white">
  <div class="col-xs-12">
    <div class="pad-xs-big type-center pad-bottom-small">
      <h4 class="type-grey"><?= $parent->title() ?></h4>
    </div>

    <div class="slideshow pad-xs-medium">
      <div data-slideshow-quotes>
        <? foreach($children as $p): ?>
        <div class="row middle-xs">
          <div class="col-xs-4 col-xs-offset-1 col-lg-3 col-lg-offset-2 pad-xs-medium">
            <div>
              <div
                class="img-square img-contain"
                <? if ($p->hasImages()) echo 'style="background-image: url(' . thumb($p->images()->first(), array('height' => 800, 'width' => 800), false) . ')"' ?>
              >
              </div>
            </div>
          </div>
          <div class="col-xs-6 pad-xs-medium <?= (isset($size) ? $size : 'type-normalish') ?>">
            <?= $p->text()->kirbytext() ?>
            <span class="type-normal"><?= $p->source() ?></span>
          </div>
        </div>
        <? endforeach; ?>
      </div>
    </div>
    <div class="margin-top-medium"></div>
  </div>
</div>

<? endif; ?>
