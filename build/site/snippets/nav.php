<a href="<?= url() ?>" class="nav-logo"></a>

<div class="nav-icon" data-nav-show>
  <div></div>
  <div></div>
  <div></div>
</div>

<nav class="primary type-white">
  <a href="<?= url() ?>" class="nav-logo"></a>
  <div class="nav-hide stack-4" data-nav-hide>&times;</div>
  <div class="row">
    <div class="block-cover">
      <div>
        <ul class="sections">

          <li>
            <div class="type-heading type-medium pad-xs-medium">
              <?= page('about')->title()->html() ?>
            </div>
            <ul class="type-caps list-inline type-condensed type-normal-medium">
              <li><a href="<?= page('about')->url() ?>"><?= page('about')->titlealt() ?></a></li>
              <li><a href="<?= page('newsroom')->url() ?>"><?= page('newsroom')->title() ?></a></li>
              <li><a href="<?= page('about')->find('partners')->url() ?>"><?= page('about')->find('partners')->title() ?></a></li>
              <li><a href="<?= page('about')->find('strategic-partner-network')->url() ?>"><?= page('about')->find('strategic-partner-network')->title() ?></a></li>
            </ul>
          </li>

          <li>
            <div class="type-heading type-medium pad-xs-medium">
              Products and Services
            </div>
            <ul class="type-caps list-inline type-condensed type-normal-medium">
              <li><a href="<?= page('game-prep')->url() ?>"><?= page('game-prep')->title() ?></a></li>
              <li><a href="<?= page('scouting-recruiting')->url() ?>"><?= page('scouting-recruiting')->title() ?></a></li>
              <li><a href="<?= page('officiating')->url() ?>"><?= page('officiating')->title() ?></a></li>
              <li><a href="<?= page('content-licensing')->url() ?>"><?= page('content-licensing')->title() ?></a></li>
            </ul>
          </li>

          <li>
            <div class="type-heading type-medium pad-xs-medium">
              <?= page('sports')->title()->html() ?>
            </div>
            <ul class="type-caps list-inline type-condensed type-normal-medium">
              <? foreach (page('sports')->children()->visible() as $subp): ?>
                <li><a href="<?= $subp->url() ?>"><?= $subp->title()->html() ?></a></li>
              <? endforeach ?>
            </ul>
          </li>

          <li>
            <div class="type-heading type-medium pad-xs-medium">
              Get In Touch
            </div>
            <ul class="type-caps list-inline type-condensed type-normal-medium">
              <li><a href="<?= page('contact')->url() ?>#tech-support">Tech Support</a></li>
              <li><a href="<?= page('contact')->url() ?>#company">Company</a></li>
              <li><a href="<?= page('contact')->url() ?>#contact">Contact</a></li>
              <li><a href="<?= page('contact')->url() ?>#tutorials">Tutorials</a></li>
            </ul>
            <ul class="type-caps list-inline type-condensed type-normal-medium">
              <li><a href="<?= $site->facebook() ?>" class="social facebook" target="_blank">Facebook</a></li>
              <li><a href="<?= $site->twitter() ?>" class="social twitter" target="_blank">Twitter</a></li>
              <li><a href="<?= $site->linkedin() ?>" class="social linkedin" target="_blank">Linkedin</a></li>
            </ul>
          </li>

        </ul>
      </div>
    </div>
  </div>
</nav>
