<?
  $sports = false;
  if ($page->isChildOf(page('sports'))) {
    $sports = true;
  }
  if ($page->is(page('about')) || $page->is(page('game-prep')) || $page->is(page('scouting-recruiting')) || $page->is(page('content-licensing')) || $page->is(page('newsroom')) || $page->is(page('officiating'))) {
    $sports = true;
  }
?>

<nav class="sub pad-xs-smallish hide-xs type-white type-caps" <? if ($sports || $page->is(page('contact'))) echo 'data-nav-secondary' ?>>
  <ul
    class="row items-center type-bold pad-x-small-children"
    data-subnav
  >
    <li><a href="<?= url() ?>">Home</a></li>
    <? foreach ($site->pages()->visible() as $p): ?>
    <li><a href="<?= $p->url() ?>"><?= $p->title() ?></a></li>
    <? endforeach; ?>
  </ul>

  <? if ($sports): ?>
  <ul
    class="subsub row items-center type-condensed pad-x-small-children"
    style="margin-top: 5px"
    data-subnav
    data-subsubnav
  >
    <? if ($page->is(page('about'))): ?>
    <li><a href="#who-we-are">Who We Are</a></li>
    <li><a href="#history">History</a></li>
    <?php endif; ?>

    <? foreach ($page->children()->visible()->not('strategic-partner-network') as $p): ?>
    <li><a href="#<?= $p->uid() ?>"><?= $p->title() ?></a></li>
    <? endforeach; ?>
  </ul>
  <? endif; ?>

  <? if ($page->is(page('contact'))): ?>
  <ul
    class="row items-center type-condensed pad-x-small-children"
    style="margin-top: 5px"
    data-subnav
    data-subsubnav
  >
    <li><a href="#tech-support">Tech Support</a></li>
    <li><a href="#company">Company</a></li>
    <li><a href="#contact">Contact Us</a></li>
    <li><a href="#tutorials">Tutorials</a></li>
  </ul>
  <? endif; ?>
</nav>
