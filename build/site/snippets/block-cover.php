<?
  $background = $p && $p->hasImages() && $p->images()->find('background.jpg')
?>

<div 
  class="block-cover type-white <? if ($background) echo 'block-dim' ?>"
  <? if ($background) echo 'style="background-image: url(' . thumb($p->images()->find('background.jpg'), array('height' => 1600, 'width' => 1600), false) . ')"' ?>
>
  <div class="stack-4 block-center">
    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
      <? if ($p) echo $p->text()->kirbytext() ?>
    </div>
  </div>

  <? if ($p->video() != ''): ?>
    <div 
      class="header-video" 
      data-video="<?= $p->video() ?>"
    >
      <div data-video-container></div>
    </div>
  <?php endif; ?>
</div>