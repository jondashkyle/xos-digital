window.jQuery = window.$ = require('jquery')
var slick = require('slick-carousel')
var SVGInjector = require('svg-injector')
var embed = require('embed-video')
var mediaSize = require('mediasize')

/**
 * Modules
 */
var typography = require('./typography')
var navigation = require('./navigation')

/**
 * Init
 */

navigation.start()

$('[data-slideshow-quotes]').slick({
  draggable: false,
  prevArrow: '<div class="arrow slick-next"><img src="/assets/images/arrow-right.svg" data-inject></div>',
  nextArrow: '<div class="arrow slick-prev"><img src="/assets/images/arrow-left.svg" data-inject></div>'
})

$('[data-slideshows-partners]').slick({
  slidesToShow: 10, 
  slidesToScroll: 8,
  responsive: [
    {
      breakpoint: 640,
      settings: {
        slidesToShow: 4, 
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 960,
      settings: {
        slidesToShow: 6, 
        slidesToScroll: 4
      }
    }
  ],
  draggable: false,
  prevArrow: '<div class="arrow slick-next"><img src="/assets/images/arrow-right.svg" data-inject></div>',
  nextArrow: '<div class="arrow slick-prev"><img src="/assets/images/arrow-left.svg" data-inject></div>'
})

typography.init()
SVGInjector($('[data-inject]'))

/**
 * Video
 */
$('body').on('click', '[data-video-inline] img', function () {
  var $video = $(this).parent().find('iframe')
  $(this).css({
    'visibility': 'hidden',
    'cursor-events': 'none'
  })
  $video.css('display', 'block')
  $video[0].src += '?autoplay=1'
})

$('nav a').on('click', navigation.hide)

/**
 * Videos
 */
$('[data-video]').each(function() {
  var id = 'video' + Math.random() * 10000
  var video = embed($(this).attr('data-video'), {query: {
    autoplay: true,
    loop: true,
    api: 1,
    player_id: id
  }})

  $(this)
    .find('[data-video-container]')
    .append(video)

  $(this)
    .find('iframe')
    .attr('id', id)
    .attr('height', 720)
    .attr('width', 1280)

  var resize = mediaSize($(this).find('iframe'), {
    parent: $(this).find('[data-video-container]'),
    size: 'cover'
  })

  var iframe = $(this).find('iframe')[0]
  var player = $f(iframe)

  player.addEvent('ready', function() {
    player.api('setVolume', 0)
  })

  player.addEvent('play', function() {
    player.api('setVolume', 0)
  })

  player.addEvent('playProgress', function() {
    player.api('setVolume', 0)
  })

  resize.start()
})

/**
 * Paginated lists
 */

$('[data-paginate-list]').each(function() {
  var $items = $(this).find('[data-paginate-list-item]')
  var $more = $(this).find('[data-paginate-list-more]')
  var offset = 0
  var limit = 6

  var paginate = function() {
    $items.slice(offset, offset + limit).css('display', '')

    if (offset + limit > $items.length) {
      $more.css('display', 'none')
    }

    offset += limit
  }

  $items.css('display', 'none')
  $more.on('click', paginate)
  paginate()
})