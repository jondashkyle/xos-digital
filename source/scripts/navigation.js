var EventEmitter = require('events').EventEmitter
var events = new EventEmitter()
var scrollTop = require('scrolltop')
var scrollMonitor = require('scrollMonitor')
var velocity = require('velocity-animate')
require('velocity-animate/velocity.ui')

var $el = $('nav.primary')
var $sub = $('nav.sub')
var $subsub = $('[data-subsubnav]')
var active = false

function show () {
  if (! active) {
    active = true
    $el.find('ul.sections > li').css('opacity', '0')
    velocity($el, 'fadeIn', 500)
    velocity($el.find('ul.sections > li'), 'transition.slideDownIn', {
      stagger: 100,
      duration: 500
    })
    $('body').addClass('scroll-disable')
  }
}

function hide () {
  if (active) {
    active = false
    velocity($el, 'fadeOut', 500)
    $('body').removeClass('scroll-disable')
  }
}

function toggle () {
  if (active) {
    hide()
  } else {
    show()
  }
}

function positionSub() {
  var top = scrollTop()
  var max = 49

  if ($sub.is('[data-nav-secondary]')) {
    max = 34
  }

  if (top > max) {
    top = max
  } else if (top < 0) {
    top = 0
  }

  $sub.css({
    '-webkit-transform': 'translateY(' + top * -1 + 'px)'
  })
}

function start () {
  $('[data-nav-toggle]').on('click', toggle)
  $('[data-nav-show]').on('click', show)
  $('[data-nav-hide]').on('click', hide)

  window.addEventListener('scroll', positionSub, false)
}

function stop () {
  $('[data-nav-toggle]').off('click', toggle)
  $('[data-nav-show]').off('click', show)
  $('[data-nav-hide]').off('click', hide)

  window.removeEventListener('scroll', positionSub, false)
}

function subnavSetup () {
  var $el = $(this)
  var $target = $('#' + $el.attr('href').slice(1))
  var elementWatcher = scrollMonitor.create($target[0])

  elementWatcher.enterViewport(function() {
    subnavActive($el)
  })

  elementWatcher.exitViewport(function() {
    subnavInactive($el)
  })
}

function subnavActive (el) {
  subnavReset()
  $(el).attr('data-active', '')
  $('[data-active]').first().addClass('active')
}

function subnavInactive (el) {
  subnavReset()
  $(el).removeAttr('data-active')
  $('[data-active]').first().addClass('active')
}

function subnavReset () {
  $subsub.find('[href]').removeClass('active')
}

exports.on = function on (ev, cb) {
  events.on(ev, cb)
}

exports.off = function off (ev, cb) {
  events.removeListener(ev, cb)
}

/**
 * Setup
 */

$subsub.find('[href]').each(subnavSetup)

/**
 * Public methods
 */

exports.show = show
exports.hide = hide
exports.toggle = toggle
exports.start = start
