require('components-webfontloader')

/**
 * Cache
 */
var $html = $('html')

/**
 *    ___ ___  _  _ _____ ____
 *   | __/ _ \| \| |_   _|_  /
 *   | _| (_) | .` | | |  / / 
 *   |_| \___/|_|\_| |_| /___|   
 *                         
 */
var WebFontConfig = {
  classes: false,
  custom: {
    families: ['Foundation Sans'],
    urls: ['/assets/fonts/fonts.css']
  },
  active: function() {
    $html.removeClass('type-loading')
  },
};

setTimeout(function() {
  $html.removeClass('type-loading')
}, 5000)

exports.init = function() {
  WebFont.load(WebFontConfig)
}